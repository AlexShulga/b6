<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
  </head>
  <body>

<?php

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.


if (!empty($messages)) {
    print('<div id="messages">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}

// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********
$user = 'u16349';
$passme = '2589362';
$db = new PDO('mysql:host=localhost;dbname=u16349', $user, $passme, array(PDO::ATTR_PERSISTENT => true));


$result = $db->prepare("SELECT SQL_CALC_FOUND_ROWS id, name FROM application2");
$result->execute();
$result = $db->prepare("SELECT FOUND_ROWS()");
$result->execute();
$row_count =$result->fetchColumn();


try {
    $stmt = $db->prepare("SELECT * from application2");
    $stmt -> execute();
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}
$result = $stmt->fetchAll();

echo "<br>Enter the row number to delete";
?>
<form action="" method="POST">
<br><label>
<input name="delete" value="<?php print $values['delete']; ?>"></input>
</label><br>
<input type="submit" value="delete"/>
</form>
<?php
echo "<table><tr><th>Id</th><th>Login</th><th>Password</th><th>Name</th><th>Email</th><th>Year</th><th>Gender</th><th>Limbs</th><th>Abilities</th><th>Biography</th></tr>";
for ($i = 0 ; $i < $row_count ; ++$i)
{
    echo "<tr>";
    echo "<td>";
    echo $result[$i]['id'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['login'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['password'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['name'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['email'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['year'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['gender'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['limbs'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['abilities'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['bio'];
    echo "</td>";
}
echo "</table>"; 
?>
</body>
</html>