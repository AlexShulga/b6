<?php


$user = 'u16349';
$pass_db = '2589362';
$db = new PDO('mysql:host=localhost;dbname=u16349', $user, $pass_db, array(PDO::ATTR_PERSISTENT => true));


// Подготовленный запрос. Не именованные метки.
try {
    $stmt = $db->prepare("SELECT id FROM admin WHERE login = ? AND password = md5(?)");
    $stmt -> execute(array($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']));
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}
$admin_id=$stmt->fetchAll();
if(empty($admin_id[0]['id']) || empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW'])){
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="My site"');
    print('<h1>401 Authorization required</h1>');
    exit();
}
else{
    if(!empty($admin_id[0]['id'])){
        print('You have successfully logged in and see password protected data.');
    }
}
header('Content-Type: text/html; charset=UTF-8');


if ($_SERVER['REQUEST_METHOD'] == 'GET'){
    // Массив для временного хранения сообщений пользователю.
    $messages = array();
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        // Если есть параметр save, то выводим сообщение пользователю.
        $messages[] = 'Deleted.';
    }
    $errors = array();
    $errors['delete'] = !empty($_COOKIE['delete_error']);
    // Выдаем сообщения об ошибках.
    if ($errors['delete']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('delete_error', '',100000);
        // Выводим сообщение.
        if($_COOKIE['delete_error'] == '1'){
            $messages[] = '<div class="error">Enter a "line number".</div>';
        }
        if($_COOKIE['delete_error'] == '2'){
            $messages[]= '<div class="error">Enter a correct "line number".</div>';
        }
    }
    $values = array();
    $values['delete'] = empty($_COOKIE['delete_value']) ? '' : $_COOKIE['delete_value'];
    include('adminform.php');
}

// Иначе, если запрос был методом POST, т.е. нужно проверить данные
else{
    print('POST');
    $errors = FALSE;
    if (empty($_POST['delete'])) {
        setcookie('delete_error', '1');
        $errors = TRUE;
    }
    else{
        try {
            $stmt = $db->prepare("SELECT id FROM application2 WHERE id = ?");
            $stmt -> execute(array($_POST['delete']));
        }
        catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
        $user_id=$stmt->fetchAll();
        if(empty($user_id[0]['id'])){
            setcookie('delete_error', '2');
            $errors = TRUE;
            //setcookie('delete_value', $_POST['delete']);
        }
        else{
            // Сохраняем ранее введенное в форму значение на год.
            setcookie('delete_value', $_POST['delete'], time() + 12 * 30 * 24 * 60 * 60);
        }
    }
    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        printf('ERRORS');
        header('Location: admin.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('delete_error', '', 100000);
    }
        //ТУТ ДЕЛАЕМ УДАЛЕНИЕ
        try {
            $stmt = $db->prepare("DELETE FROM application2 WHERE id = ?");
            $stmt -> execute(array($_POST['delete']*1));
        }
        catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
     
    setcookie('save', '1');
    header('Location: admin.php');
}
