<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$abilities = ['not'=>'not','immortality'=>'immortality','telepathy'=>'telepathy','levitation'=>'levitation','teleportation'=>'teleportation'];
for ($i=1900;$i<=2020;$i++){
    $year[$i] = $i;
}
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Выводим сообщение пользователю.
    //$messages[] = 'Thanks, the results are saved.';
    
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
       $messages[] = sprintf('You can <a href="login.php">login</a> with username <strong>%s</strong>
        and password <strong>%s</strong> to change data.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
    else{
        $messages[] = 'Thanks, the results are saved. You can <a href="login.php">exit</a>.';
    }
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email']=!empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);
  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('fio_error', '',100000);
      // Выводим сообщение.
      if($_COOKIE['fio_error'] == '1'){
          $messages[] = '<div class="error">Enter a "name".</div>';
      }
      if($_COOKIE['fio_error'] == '2'){
          $messages[]= '<div class="error">Enter a correct "name" in English.</div>';
      }
  }
  // TODO: тут выдать сообщения об ошибках в других полях.
  // Выдаем сообщения об ошибках.
  
  // Выдаем сообщения об ошибках.
  if ($errors['email']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('email_error', '',100000);
      // Выводим сообщение.
      if($_COOKIE['email_error'] == '1'){
          $messages[] = '<div class="error">Enter a "E-mail".</div>';
      }
      if($_COOKIE['email_error'] == '2'){
          $messages[]= '<div class="error">Enter a correct "E-mail".</div>';
      }
  }
  
  if ($errors['year']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('year_error', '', 100000);
      // Выводим сообщение.
      if ($_COOKIE['year_error'] == '1'){
          $messages[] = '<div class="error">Enter a "year".</div>';
      }
  }
  
  if ($errors['gender']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('gender_error', '', 100000);
      // Выводим сообщение.
      if ($_COOKIE['gender_error']){
          $messages[] = '<div class="error">Enter a "gender".</div>';
      }
  }
  
  if ($errors['limbs']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('limbs_error', '', 100000);
      // Выводим сообщение.
      if ($_COOKIE['limbs_error']){
          $messages[] = '<div class="error">Enter a "limbs".</div>';
      }
  }
  
  if ($errors['abilities']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('abilities_error', '', 100000);
      // Выводим сообщение.
      if ($_COOKIE['abilities_error'] == '1'){
          $messages[] = '<div class="error">Enter a "abilities".</div>';
      }
  }
  
  if ($errors['bio']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('bio_error', '',100000);
      // Выводим сообщение.
      if($_COOKIE['bio_error'] == '1'){
          $messages[] = '<div class="error">Enter a "biography".</div>';
      }
      if($_COOKIE['bio_error'] == '2'){
          $messages[]= '<div class="error">Enter a correct "biography" in English.</div>';
      }
  }
  if ($errors['check']) {
      setcookie('check_error', '', 100000);
      $messages[] = '<div class="error">Enter a "accept".</div>';
  }

  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  // TODO: аналогично все поля.
  $values['email'] = (empty($_COOKIE['email_value'])) ? '' : strip_tags($_COOKIE['email_value']);
  if(!empty($_COOKIE['year_value'])){
      $year_value = json_decode(strip_tags($_COOKIE['year_value']));
  }
  $values['year'] = [];
  if(isset($year_value) && is_array($year_value)){
      foreach ($year_value as $year1){
          if(!empty($year[$year1])){
              $values['year'][$year1] = strip_tags($year1);    
          }
      }
  }
  
  $values['gender'] = (empty($_COOKIE['gender_value'])) ? '' : strip_tags($_COOKIE['gender_value']);
  $values['limbs'] = (empty($_COOKIE['limbs_value'])) ? '' : strip_tags($_COOKIE['limbs_value']);
  
  if(!empty($_COOKIE['abilities_value'])){
      $abilities_value = json_decode(strip_tags($_COOKIE['abilities_value']));
  }
  $values['abilities'] = [];
  if(isset($abilities_value) && is_array($abilities_value)){
      foreach ($abilities_value as $ability){
          if(!empty($abilities[$ability])){
              $values['abilities'][$ability] = strip_tags($ability);            
          }
      }
  }
  
  $values['bio'] = (empty($_COOKIE['bio_value'])) ? '' : strip_tags($_COOKIE['bio_value']);
  $values['check'] = (empty($_COOKIE['check_value'])) ? '' : strip_tags($_COOKIE['check_value']);
  

  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (/*empty($errors) &&*/ !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
          /*$messages[] = */
    // TODO: загрузить данные пользователя из БД                                                                !!!!!
    // и заполнить переменную $values,                                                                          !!!!!
    // предварительно санитизовав.                                                                              !!!!!
          printf('Login with username %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
         // $messages[] = printf('Login with username %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
          $user = 'u16349';
          $pass_db = '2589362';
          $db = new PDO('mysql:host=localhost;dbname=u16349', $user, $pass_db, array(PDO::ATTR_PERSISTENT => true));
          
          $l=$_SESSION['login'];
          $p=$_SESSION['pass'];
          $stmt = $db->prepare("SELECT name, email, year, gender, limbs, abilities, bio FROM application2 WHERE login= ? and password=?");
          $stmt->execute(array($l, $p));
          $result = $stmt->fetchAll();
          $values['fio']=$result[0]['name'];
          $values['email']=$result[0]['email'];
          $values['gender']=$result[0]['gender'];
          $values['limbs']=$result[0]['limbs'];
          $values['bio']=$result[0]['bio'];
          $a_value=explode(", ", $result[0]['abilities']);
          $values['abilities']=[];
          foreach ($a_value as $i){
              if(!empty($abilities[$i])){
              $values['abilities'][$i] = $i;
             }
          }
  $y_value = explode(", ", $result[0]['year']);
  $values['year'] = [];
  foreach ($y_value as $j){
      if(!empty($year[$j])){
          $values['year'][$j] = $j;
      }
  }
      }
  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1');
        $errors = TRUE;
    }
    else{
        if(!preg_match('/^[a-zA-Z]/',$_POST['fio'])){
            setcookie('fio_error', '2');
            $errors = TRUE;
            setcookie('fio_value', $_POST['fio']);
        }
        else{
            // Сохраняем ранее введенное в форму значение на год.
            setcookie('fio_value', $_POST['fio'], time() + 12 * 30 * 24 * 60 * 60);
        }
    }
    
    // *************
    // TODO: тут необходимо проверить правильность заполнения всех остальных полей.
    if (empty($_POST['email'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('email_error', '1');
        $errors = TRUE;
    }
    else{
        if(!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i",$_POST['email'])){
            setcookie('email_error', '2');
            $errors = TRUE;
            setcookie('email_value', $_POST['email']);
        }
        else{
            // Сохраняем ранее введенное в форму значение на год.
            setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
        }
    }
    
    if (empty($_POST['year'])) {
        // Выдаем куку на день с флажком об ошибке в поле name.
        setcookie('year_error', '1');
        $errors = TRUE;
    }
    else {
        $year_error = FALSE;
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('year_value', json_encode($_POST['year']), time() + 12 * 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['gender'])) {
        // Выдаем куку на день с флажком об ошибке в поле name.
        setcookie('gender_error', '1');
        $errors = TRUE;
    }
    else{
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('gender_value', $_POST['gender'], time() + 12 * 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['limbs'])) {
        // Выдаем куку на день с флажком об ошибке в поле name.
        setcookie('limbs_error', '1');
        $errors = TRUE;
    }
    else{
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('limbs_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['abilities'])) {
        // Выдаем куку на день с флажком об ошибке в поле name.
        setcookie('abilities_error', '1'/*, time() + 24 * 60 * 60*/);
        $errors = TRUE;
    }
    else {
        $abilities_error = FALSE;
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('abilities_value', json_encode($_POST['abilities']), time() + 12 * 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['bio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('bio_error', '1');
        $errors = TRUE;
    }
    else{
        if(!preg_match('/^[a-zA-Z1-9]/',$_POST['bio'])){
            setcookie('bio_error', '2');
            $errors = TRUE;
            setcookie('bio_value', $_POST['bio']);
        }
        else{
            // Сохраняем ранее введенное в форму значение на год.
            setcookie('bio_value', $_POST['bio'], time() + 12 * 30 * 24 * 60 * 60);
        }
    }
    if (empty($_POST['check'])) {
        // Выдаем куку на день с флажком об ошибке в поле name.
        setcookie('check_error', '1');
        $errors = TRUE;
    }
    else{
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('check_value', $_POST['check'], time() + 12 * 30 * 24 * 60 * 60);
    }
    

// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
    setcookie('email_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('gender_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('bio_error', '', 100000);
    setcookie('check_error', '', 100000);
    //$errors = array();
  }

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
          $messages[] = sprintf('You can <a href="login.php">exit</a>');
    // TODO: перезаписать данные в БД новыми данными,                                                   !!!!!
    // кроме логина и пароля.
          $user = 'u16349';
          $pass_db = '2589362';
          $db = new PDO('mysql:host=localhost;dbname=u16349', $user, $pass_db, array(PDO::ATTR_PERSISTENT => true));
          
          $abilities_bd = array();
          foreach ($_POST['abilities'] as $key => $value) {
              $abilities_bd[$key] = $value;
          }
          $abilities_string = implode(', ', $abilities_bd);
          
          $year_bd = array();
          foreach ($_POST['year'] as $key => $value) {
              $year_bd[$key] = $value;
          }
          $year_string = implode(', ', $year_bd);
          
          $log=strip_tags($_SESSION['login']);
          $pas=$_SESSION['pass'];
          // Подготовленный запрос. Не именованные метки.
          $stmt = $db->prepare("UPDATE application2 SET name = ?, email = ?, year = ?, gender = ?, limbs = ?, abilities = ?, bio = ? WHERE login = ? AND password = ?");
          $stmt->execute(array($_POST['fio'], $_POST['email'], $year_string, $_POST['gender'], $_POST['limbs'], $abilities_string, $_POST['bio'] , $log, $pas));
          
  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
    //$login = '123';
      $login = $_POST['email'];
      $pass = rand(1000, 9999);
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('pass', $pass);

    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    // ...
  }
  $user = 'u16349';
  $pass_db = '2589362';
  $db = new PDO('mysql:host=localhost;dbname=u16349', $user, $pass_db, array(PDO::ATTR_PERSISTENT => true));
  
  
  
  $abilities_bd = array();
  foreach ($_POST['abilities'] as $key => $value) {
      $abilities_bd[$key] = $value;
  }
  $abilities_string = implode(', ', $abilities_bd);
  
  $year_bd = array();
  foreach ($_POST['year'] as $key => $value) {
      $year_bd[$key] = $value;
  }
  $year_string = implode(', ', $year_bd);
  // Подготовленный запрос. Не именованные метки.
  try {
      $stmt = $db->prepare("INSERT INTO application2 SET login = ?, password = ?, name = ?, email = ?, year = ?, gender = ?, limbs = ?, abilities = ?, bio = ?");
      $stmt -> execute(array($login, md5($pass), $_POST['fio'], $_POST['email'], $year_string, $_POST['gender'], $_POST['limbs'], $abilities_string, $_POST['bio']));
  }
  catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
  }
  
  
  
  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: ./');
}
