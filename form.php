<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
    <link rel="stylesheet" href="css2.css"> 
    <title>Shulga</title>
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
  </head>
  <body>

<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
<div class="middle">
<p><div class="menu">I'm <a href="admin.php">admin</a></div><div class="menu1"> You can <a href="login.php">login</a></div></p>
<p><h1>Profile</h1>
    <form action="" method="POST">
    <div id="fancy-inputs">
    <p>
    
    <label class="input">
    <h2 <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>">Name</h2>
      <input name="fio" class="input" value="<?php print $values['fio']; ?>"></input><br>
      </label><br></p><br>
      <p>
       <label class="input">
       <h2 <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>">E-mail:</h2>
            <input name="email" class="input" value="<?php print $values['email']; ?>">
          </label></p>
          </div>
      <label>
      
      <h2>Year of birth:</h2><br>
          
          <select name="year[]" <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>" class="btn1">
              <?php 
              foreach ($year as $key => $value){
                  $selected = empty($values['year'][$key]) ? '' : 'selected="selected"';
                  printf('<option value="%s"%s>%s</option>', $key, $selected, $value);
              }
              ?>
              </select>
          
          </label>
          <h2 <?php if ($errors['gender']) {print 'class="error"';} ?> value="<?php print $values['gender']; ?>">Gender</h2>
          <input type="radio" name="gender" value="male" <?php if ($values['gender']=="male") {print 'checked="checked"';}?>/><div class="r">male</div>
		  <input type="radio" name="gender" value="female" <?php if ($values['gender']=="female") {print 'checked="checked"';}?>/><div class="r">female</div>
          
          <h2 <?php if ($errors['limbs']) {print 'class="error"';} ?> value="<?php print $values['limbs']; ?>">Number of limbs:</h2>
          <div class="fancy-radio">
            <!-- label>
              <input type="radio" name="limbs" value="0" <?php if ($values['limbs']==0) {print 'checked="checked"';}?>><div class="r"> 0</div>
            </label> -->
            <label>
              <input type="radio" name="limbs" value="1" <?php if ($values['limbs']==1) {print 'checked="checked"';}?>><div class="r"> 1</div>
            </label>
            <label>
              <input type="radio" name="limbs" value="2" <?php if ($values['limbs']==2) {print 'checked="checked"';}?>><div class="r"> 2</div>
            </label>
            <label>
              <input type="radio" name="limbs" value="3" <?php if ($values['limbs']==3) {print 'checked="checked"';}?>><div class="r"> 3</div>
            </label>
            <label>
              <input type="radio" name="limbs" value="4" <?php if ($values['limbs']==4) {print 'checked="checked"';}?>><div class="r"> 4</div>
            </label>
          </div>
            <h2 <?php if ($errors['abilities']) {print 'class="error"';} ?>>Super abilities:</h2><br>
              <select name="abilities[]" multiple="multiple" class="btn2" value="<?php print $values['abilities']; ?>">
              <?php 
              foreach ($abilities as $key => $value){
                  $selected = empty($values['abilities'][$key]) ? '' : 'selected="selected"';
                  printf('<option value="%s"%s>%s</option>', $key, $selected, $value);
              }
              ?>
              </select>
          
          <h2 <?php if ($errors['bio']) {print 'class="error"';} ?> value="<?php print $values['bio']; ?>">Biography:</h2><br>
          <textarea name="bio"class="btn2" value="<?php print $values['bio']; ?>"><?php print($values['bio']); ?></textarea><br>
          <label <?php if ($errors['check']) {print 'class="error"';} ?>><input type="checkbox" name="check" value="accept"
          <?php if ($values['check']=="accept") {print 'checked="checked"';}?>><div class="r"> Accept</div>
          </label><br>
      <input type="submit" value="submit" class="btn"/>
    </form></p>
  </body>
</html>